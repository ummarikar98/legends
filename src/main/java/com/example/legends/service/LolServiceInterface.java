package com.example.legends.service;

import com.example.legends.model.LolChampion;

import java.util.List;

public interface LolServiceInterface {
    List<LolChampion> findAll();
    LolChampion save(LolChampion lolChampion);
}
