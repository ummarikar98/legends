package com.example.legends.service;

import com.example.legends.model.LolChampion;
import com.example.legends.repository.LolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LolService implements LolServiceInterface {

    @Autowired
    private LolRepository lolRepository;

    public List<LolChampion> findAll() {
        return lolRepository.findAll();
    }

    public LolChampion save(LolChampion lolChampion) {
        return lolRepository.save(lolChampion);
    }
}
