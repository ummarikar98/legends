package com.example.legends.rest;

import com.example.legends.model.LolChampion;
import com.example.legends.service.LolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LolController {

    @Autowired
    private LolService lolService;

    public List<LolChampion> findAll() {
        return lolService.findAll();
    }
}
