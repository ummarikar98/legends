package com.example.legends.repository;

import com.example.legends.model.LolChampion;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface LolRepository extends MongoRepository<LolChampion, String> {

}
